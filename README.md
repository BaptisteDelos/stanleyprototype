# The Stanley Prototype

This project has been developed during an CG applied course of my formation. Aside from minor revisions, the code of this prototype has been left as is, now used as a basis for a <a href="https://gitlab.com/BaptisteDelos/TheStanleyPrototype-Chapter2" target="_blank">second episode</a>.  
To use it, you are invited to follow the steps below in order of succession.

## Load submodules

```
git submodule init  
git submodule update
```

## Build Stanley

```
mkdir build && cd build  
cmake ..  
make -j $(nproc)
```

## Execute Stanley

The main application will be available in the *bin* folder created at the project root.  
Do what you have to do then.

## Shortcuts

|       |                                           |
|:-----:| ----------------------------------------- |
| **p** | Switch between Euler and Trackball camera |
| **w** | Switch between wireframe and filled drawing |
| **c** | Default rendering mode |
| **n** | Normal mode |
| **d** | Diffuse mode |
| **e** | LBS weights mode |
| **q** | Linear Blend Skinning |