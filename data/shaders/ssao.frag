#version 410 core

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D texNoise;

uniform vec3 samples[64];
uniform mat4 projection;

layout (location = 0) out float FragColor;

const vec2 noiseScale = vec2(1920.0/4.0, 1080.0/4.0);

void main()
{
    vec3 FragPos = texture(gPosition, TexCoords).xyz;
    vec3 Normal = texture(gNormal, TexCoords).rgb;
    // Adapt noise texture coordinates to screen resolution divided by noise kernel width
    vec3 randVec = normalize(texture(texNoise, TexCoords * noiseScale).xyz);

    // Determine TBN : tangent space -> view space
    vec3 tangent = normalize(randVec - dot(randVec, Normal) * Normal);
    vec3 bitangent = cross(Normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, Normal);

    // Compute fragment occlusion
    float occlusion = 0.0;
    int kernelSize = 64;
    float radius = 0.5;

    for (int i = 0; i < kernelSize; ++i)
    {
        vec3 viewSample = TBN * samples[i];
        viewSample = FragPos + viewSample * radius;

        vec4 offset = vec4(viewSample, 1.0);
        offset = projection * offset;
        offset.xyz /= offset.w;
        offset.xyz = offset.xyz * 0.5 + 0.5;

        // Retrieve sample depth from its view-space position
        float sampleDepth = texture(gPosition, offset.xy).z;

        // Void contribution of samples outside the normal-oriented hemisphere
        float rangeCheck = smoothstep(0.0, 1.0, radius/abs(FragPos.z - sampleDepth));

        occlusion += rangeCheck * (sampleDepth >= viewSample.z + 0.025 ? 1.0 : 0.0);
    }

    occlusion = 1.0 - (occlusion / kernelSize);

//    FragColor = vec4(occlusion, occlusion, occlusion, 1.0);
    FragColor = occlusion;
}
