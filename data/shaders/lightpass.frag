#version 410 core

in vec2 TexCoords;

layout (location = 0) out vec4 FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D ssaoTexture;

void main()
{
    float ssao = texture(ssaoTexture, TexCoords).r;
    FragColor = vec4(texture(gAlbedoSpec, TexCoords).rgb, 1.0f);
//    FragColor *= ssao;
//    FragColor = vec4(0.7f, 0.5f, 0.9f, 1.0f);
//    gAlbedoSpec.rgb = texture(texture_diffuse1, TexCoords).rgb;
//    gAlbedoSpec.a = texture(texture_specular1, TexCoords).r;
}

