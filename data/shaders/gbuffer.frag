#version 410 core

in vec3 FragPos;
in vec3 Normal;
in vec3 Color;
in vec2 TexCoords;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

void main()
{
    gPosition = FragPos;
    gNormal = normalize(Normal);
    gAlbedoSpec = vec4(0.95, 0.95, 0.95, 1.0);
//    gAlbedoSpec = vec4(0.89, 0.62, 0.95, 1.0);
}

