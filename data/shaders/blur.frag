#version 410 core

in vec2 TexCoords;

uniform sampler2D ssaoInput;

layout (location = 0) out float FragColor;

void main()
{
    vec2 texelSize = 1.0/vec2(textureSize(ssaoInput, 0));
    FragColor = 0.0;
    for (int i = -2; i < 2; ++i)
    {
        for (int j = -2; j < 2; ++j)
        {
            vec2 offset = vec2(float(i), float(j)) * texelSize;
            FragColor += texture2D(ssaoInput, TexCoords + offset).r;
        }
    }

    FragColor /= (4.0 * 4.0);
}
