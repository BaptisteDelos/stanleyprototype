#version 410 core

layout (location = 0) in vec3 iposition;
layout (location = 1) in vec2 itexcoord;

out vec2 TexCoords;

void main()
{
    gl_Position = vec4(iposition.x, iposition.y, 0.0, 1.0);
    TexCoords = itexcoord;
}
