#version 410 core

in vec3 Normal;
out vec4 FragColor;

uniform float weight;

void main()
{
    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
}
