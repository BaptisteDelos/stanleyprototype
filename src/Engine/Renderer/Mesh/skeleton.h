#ifndef SKELETON_H
#define SKELETON_H

#include <Engine/Renderer/opengl_stuff.h>
#include <Engine/Renderer/Mesh/bone.h>


class Skeleton
{
public:
    Skeleton();

    void draw(Shader &shader);

    void addBone(Bone& bone);
    std::vector<Bone> bones() const;
    float length(void) const;
    float lengthTo(int boneIndex) const;
    void updateTransforms(void);

private:
    std::vector<Bone> _bones;
};

#endif // SKELETON_H
