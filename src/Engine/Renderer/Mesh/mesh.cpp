#include "mesh.h"

using namespace Eigen;


Mesh::Mesh() {

}

Mesh::Mesh(std::vector<Vector3f> &vertices, std::vector<GLuint> &indices, std::vector<Vector3f> &normals, std::vector<Vector2f> &texcoords) {
    initProperties();
    init(vertices, indices, normals, texcoords);

    resetProperties();

    // Animation Setup
    initLBS();

    updateVBO();
}

Mesh::~Mesh() {

}

void Mesh::init(std::vector<Vector3f> &vertices, std::vector<GLuint> &indices, std::vector<Vector3f> &normals, std::vector<Vector2f> &texcoords)
{
    VertexHandle vhandle[vertices.size()];

    // Set positions and normals
    for (int i = 0; i < vertices.size(); ++i)
    {
        Vector3f v(vertices[i][0], vertices[i][1], vertices[i][2]);
        vhandle[i] = add_vertex(Point(v[0], v[1], v[2]));

        if (!normals.empty())
        {
            request_vertex_normals();
            MyMesh::Normal n(normals[i][0], normals[i][1], normals[i][2]);
            set_normal(vhandle[i], n);
        }

        if (!texcoords.empty())
        {
            request_vertex_texcoords2D();
            MyMesh::TexCoord2D uv(texcoords[i][0], texcoords[i][1]);
            set_texcoord2D(vhandle[i], uv);
        }
    }

    std::vector<MyMesh::VertexHandle> face_vhandles;

    // Set triangular faces
    for (int i = 0; i < indices.size() - 2; i += 3)
    {
        add_face(vhandle[indices[i]], vhandle[indices[i+1]], vhandle[indices[i+2]]);
    }

    _glIndices = indices;

    initVBO();
}

void Mesh::initVBO()
{
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_nbo);
    glGenBuffers(1, &_tbo);
    glGenBuffers(1, &_cbo);
    glGenBuffers(1, &_ebo);
    glGenBuffers(1, &_wbo);
}

void Mesh::initProperties()
{
    // New vertices flag
    add_property(_vertexIsNew);
    // Position of the newly inserted vertices
    add_property(_vertexNewPosition);
    // New edges flag
    add_property(_edgeIsNew);
    // Position of the newly inserted vertex at each edge
    add_property(_edgeNewPosition);
    // New faces flag
    add_property(_faceIsNew);

    // Add a weight for each vertex to animate
    add_property(_vertexWeights);
}

void Mesh::updateVBO()
{
    _glVertices.clear();
    _glNormals.clear();
    _glIndices.clear();
    _glColors.clear();
    _glWeights.clear();

    for (MyMesh::VertexIter vit = vertices_begin(); vit != vertices_end(); ++vit)
    {
        MyMesh::Point p = point(*vit);

        _glVertices.emplace_back(p[0]);
        _glVertices.emplace_back(p[1]);
        _glVertices.emplace_back(p[2]);

        if (has_vertex_normals())
        {
            MyMesh::Normal n = normal(*vit);

            _glNormals.emplace_back(n[0]);
            _glNormals.emplace_back(n[1]);
            _glNormals.emplace_back(n[2]);
        }
        if (has_vertex_colors())
        {
            MyMesh::Color c = color(*vit);

            _glColors.emplace_back(c[0]);
            _glColors.emplace_back(c[1]);
            _glColors.emplace_back(c[2]);
        }
        if (has_vertex_texcoords2D())
        {
            MyMesh::TexCoord2D uv = texcoord2D(*vit);

            _glTexcoord.emplace_back(uv[0]);
            _glTexcoord.emplace_back(uv[1]);
        }
        if (_LBSComputed)
        {
            Vector4f weights = property(_vertexWeights, *vit);
            for (int i = 0; i < 4; ++i) { _glWeights.emplace_back(weights[i]); }
//            for (float w : weights) { _glWeights.emplace_back(w); }
        }
    }

    for (MyMesh::FaceIter fit = faces_begin(); fit != faces_end(); ++fit)
    {
        for (MyMesh::FaceVertexIter fvit = fv_iter(*fit); fvit; ++fvit)
        {
            _glIndices.emplace_back(fvit->idx());
        }
    }

    glBindVertexArray(_vao);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _glVertices.size() * sizeof(GLfloat), _glVertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, _nbo);
    glBufferData(GL_ARRAY_BUFFER, _glNormals.size() * sizeof(GLfloat), _glNormals.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, _tbo);
    glBufferData(GL_ARRAY_BUFFER, _glTexcoord.size() * sizeof(GLfloat), _glTexcoord.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, _cbo);
    glBufferData(GL_ARRAY_BUFFER, _glColors.size() * sizeof(GLfloat), _glColors.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, _wbo);
    glBufferData(GL_ARRAY_BUFFER, _glWeights.size() * sizeof(GLfloat), _glWeights.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _glIndices.size() * sizeof(GLuint), _glIndices.data(), GL_STATIC_DRAW);

    glBindVertexArray(0);

}

void Mesh::resetProperties()
{
    for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
    {
        property(_vertexIsNew, *v_it) = false;
        property(_vertexWeights, *v_it).Zero(4, 1);
    }
    for (EdgeIter e_it = edges_begin(); e_it != edges_end(); ++e_it)
    {
        property(_edgeIsNew, *e_it) = false;
    }
    for (FaceIter f_it = faces_begin(); f_it != faces_end(); ++f_it)
    {
        property(_faceIsNew, *f_it) = false;
    }
}

void Mesh::draw(Shader shader)
{
    GLint position_loc = -1, normal_loc = -1, uv_loc = -1, color_loc = -1, weights_loc =-1;

    glBindVertexArray(_vao);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    position_loc = shader.getAttribLocation("iposition");
    glVertexAttribPointer(position_loc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *)0);
    glEnableVertexAttribArray(position_loc);

    if (has_vertex_normals() && (normal_loc = shader.getAttribLocation("inormal")) != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, _nbo);
        glVertexAttribPointer(normal_loc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *)0);
        glEnableVertexAttribArray(normal_loc);
    }

    if (has_vertex_texcoords2D() && (uv_loc = shader.getAttribLocation("itexcoord")) != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, _tbo);
        glVertexAttribPointer(uv_loc, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void *)0);
        glEnableVertexAttribArray(uv_loc);
    }

    if (has_vertex_colors() && (color_loc = shader.getAttribLocation("icolor")) != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, _cbo);
        glVertexAttribPointer(color_loc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *)0);
        glEnableVertexAttribArray(color_loc);
    }

    if (_LBSComputed && (weights_loc = shader.getAttribLocation("iweights")) != -1)
    {
        GLint nbones = GLint(_skeleton.bones().size());
        glBindBuffer(GL_ARRAY_BUFFER, _wbo);
        glVertexAttribPointer(weights_loc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void *)0);
        glEnableVertexAttribArray(weights_loc);

        sendBlendingMatricesToShader(shader);
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glDrawElements(GL_TRIANGLES, _glIndices.size(),  GL_UNSIGNED_INT, 0);

	if (position_loc != -1)
	{
    	glDisableVertexAttribArray(position_loc);
	}
	if (normal_loc != -1)
	{
	    glDisableVertexAttribArray(normal_loc);
	}
	if (uv_loc != -1)
	{
	    glDisableVertexAttribArray(uv_loc);
	}
	if (color_loc != -1)
	{
	    glDisableVertexAttribArray(color_loc);
	}
	if (weights_loc != -1)
	{
	    glDisableVertexAttribArray(weights_loc);
	}

    glBindVertexArray(0);

    /* Draw mesh skeleton */

    if (hasSkeleton())
        _skeleton.draw(shader);
}

void Mesh::load(const std::string &filename)
{

}

void Mesh::updateBoundingBox()
{
    _bbox.setNull();
    for(VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it) {
        Point p = point(*v_it);
        _bbox.extend(Vector3f(p.data()));
    }
}

void Mesh::upSample()
{
    // Compute even vertices new positions and store it
    for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
    {
        Point vNewPos;

        if (!is_boundary(*v_it))
        {
            int valence = 0;
            Point sumNei(0.0f);

            for (VertexVertexIter vv_it = vv_iter(*v_it); vv_it; ++vv_it)
            {
                sumNei += point(*vv_it);
                ++valence;
            }
            float beta = 3.0f/(8.0f*(float)valence);
            vNewPos = point(*v_it) * (1 - valence * beta) + sumNei * beta;
        }
        else
        {
            // Determine the two boundary neighbors of the current vertex
            VertexOHalfedgeIter voh_it = voh_iter(*v_it);
            for (; voh_it && !is_boundary(*voh_it); ++voh_it) {};
            VertexHandle vha = to_vertex_handle(*voh_it);
            VertexHandle vhb = to_vertex_handle(opposite_halfedge_handle(*voh_it));
            Point a = point(vha);
            Point b = point(vhb);

            vNewPos = (1./8.) * (a + b) + (3./4.) * point(*v_it);
        }
        // Store vertex new position
        property(_vertexNewPosition, *v_it) = vNewPos;
    }

    // Compute odd vertices new positions and store it
    for (EdgeIter e_it = edges_begin(); e_it != edges_end(); ++e_it)
    {
        Point eNewPos;
        HalfedgeHandle heh = halfedge_handle(*e_it, 0);
        Point a = point(to_vertex_handle(heh));
        Point b = point(from_vertex_handle(heh));

        if (!is_boundary(*e_it))
        {
            Point c = point(to_vertex_handle(next_halfedge_handle(heh)));
            Point d = point(to_vertex_handle(next_halfedge_handle(opposite_halfedge_handle(heh))));

            eNewPos = (3./8.) * (a + b) + (1./8.) * (c + d);
        }
        else
        {
            eNewPos = (1./2.) * (a + b);
        }

        // Store the new position of the edge-splitting vertex
        property(_edgeNewPosition, *e_it) = eNewPos;
    }

    EdgeIter currentEdge = edges_begin();

    // Split edges
    for (int i = 0; i < (int) n_edges(); i++)
    {
        // Save the next edge
        EdgeIter nextEdge = currentEdge;
        nextEdge++;
        EdgeHandle eh(*currentEdge);

        // Split edge if it has not just been created
        if (!property(_edgeIsNew, eh))
            split_edge(eh);

        currentEdge = nextEdge;
    }

    // Create new faces
    for (FaceIter f_it = faces_begin(); f_it != faces_end(); ++f_it)
    {
        // Split face if it has not just been created
        if (!property(_faceIsNew, *f_it))
        {
            HalfedgeHandle heh1(halfedge_handle(*f_it));
            HalfedgeHandle heh2(next_halfedge_handle(next_halfedge_handle(heh1)));
            HalfedgeHandle heh3(next_halfedge_handle(next_halfedge_handle(heh2)));

            create_face(heh1);
            create_face(heh2);
            create_face(heh3);
        }
    }

    // Finally update vertices positions
    for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
    {
        set_point(*v_it, property(_vertexNewPosition, *v_it));
    }

    update_normals();
//    update_vertex_normals();
}

void Mesh::create_face(HalfedgeHandle& heh)
{
    // Define Halfedge Handles
    HalfedgeHandle heh1(heh);
    HalfedgeHandle heh5(heh1);
    HalfedgeHandle heh6(next_halfedge_handle(heh1));

    // Cycle around the polygon to find correct Halfedge
    for (; next_halfedge_handle(next_halfedge_handle(heh5)) != heh1; heh5 = next_halfedge_handle(heh5)) {}

    VertexHandle vh1 = to_vertex_handle(heh1);
    VertexHandle vh2 = to_vertex_handle(heh5);

    HalfedgeHandle heh2(next_halfedge_handle(heh5));
    HalfedgeHandle heh3(new_edge(vh1, vh2));
    HalfedgeHandle heh4(opposite_halfedge_handle(heh3));

    // Old and new Face
    FaceHandle fh_old(face_handle(heh6));
    FaceHandle fh_new(new_face());

    property(_faceIsNew, fh_new) = true;

    // Re-Set Handles around old Face
    set_next_halfedge_handle(heh4, heh6);
    set_next_halfedge_handle(heh5, heh4);

    set_face_handle(heh4, fh_old);
    set_face_handle(heh5, fh_old);
    set_face_handle(heh6, fh_old);
    set_halfedge_handle(fh_old, heh4);

    // Re-Set Handles around new Face
    set_next_halfedge_handle(heh1, heh3);
    set_next_halfedge_handle(heh3, heh2);

    set_face_handle(heh1, fh_new);
    set_face_handle(heh2, fh_new);
    set_face_handle(heh3, fh_new);

    set_halfedge_handle(fh_new, heh1);
}

/**
 * Edge-splitting function based on OpenMesh split_edge function
 */
OpenMesh::VertexHandle& Mesh::split_edge(EdgeHandle &eh)
{
    HalfedgeHandle heh = halfedge_handle(eh, 0);
    HalfedgeHandle opp_heh = halfedge_handle(eh, 1);

    HalfedgeHandle new_heh, opp_new_heh, t_heh;
    VertexHandle vh;
    VertexHandle vh1(to_vertex_handle(heh));
    VertexHandle vh2(to_vertex_handle(opp_heh));
    Point midpoint = (point(vh1) + point(vh2)) * 0.5f;

    // Get handle of middle point
    vh = new_vertex(midpoint);

    // Set midpoint position to the position registered on the edge
    property(_vertexNewPosition, vh) = property(_edgeNewPosition, eh);
    // Flag middle point as new vertex
    property(_vertexIsNew, vh) = true;

    // Re-link mesh entities
    if (is_boundary(eh))
    {
        for (t_heh = heh; next_halfedge_handle(t_heh) != opp_heh; t_heh = opposite_halfedge_handle(next_halfedge_handle(t_heh))) {}
    }
    else
    {
        for (t_heh = next_halfedge_handle(opp_heh); next_halfedge_handle(t_heh) != opp_heh; t_heh = next_halfedge_handle(t_heh)) {}
    }

    new_heh = new_edge(vh, vh1);
    opp_new_heh = opposite_halfedge_handle(new_heh);
    set_vertex_handle(heh, vh);

    // Flag created edge as new edge
    property(_edgeIsNew, edge_handle(new_heh)) = true;

    set_next_halfedge_handle(t_heh, opp_new_heh);
    set_next_halfedge_handle(new_heh, next_halfedge_handle(heh));
    set_next_halfedge_handle(heh, new_heh);
    set_next_halfedge_handle(opp_new_heh, opp_heh);

    if (face_handle(opp_heh).is_valid())
    {
        set_face_handle(opp_new_heh, face_handle(opp_heh));
        set_halfedge_handle(face_handle(opp_new_heh), opp_new_heh);
    }

    set_face_handle( new_heh, face_handle(heh) );
    set_halfedge_handle( vh, new_heh);
    set_halfedge_handle( face_handle(heh), heh );
    set_halfedge_handle( vh1, opp_new_heh );

    // An outgoing halfedge of a boundary vertex is always a boundary halfedge
    adjust_outgoing_halfedge( vh );
    adjust_outgoing_halfedge( vh1 );

    return vh;
}

float Mesh::distOnBone(Bone &bone, Vector3f &p)
{
    std::array<Vector3f, 2> endpoints = bone.endpoints();
    Vector3f bone_segment = endpoints[1] - endpoints[0];
    float projRatio = (p - endpoints[0]).dot(bone_segment);
    Vector3f proj = projRatio * bone_segment;

    return glm::clamp(proj.norm() * glm::sign(projRatio), 0.0f, bone_segment.norm());
}

bool Mesh::hasSkeleton()
{
    return !_skeleton.bones().empty();
}

void Mesh::computeSkeleton()
{
    // Static bone
    Bone b0(Vector3f(-1.5f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 0.0f), Eigen::Matrix4f::Identity(), Eigen::Matrix4f::Identity());

    // Animated bone
    Eigen::Matrix4f transform(Eigen::Matrix4f::Identity());
    Eigen::AngleAxisf aa(90.0f * degreeRadianRatio, Vector3f(0.0f, 0.0f, 1.0f));
    transform.block<3,3>(0,0) = aa.toRotationMatrix();
    Bone b1(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(1.5f, 0.0f, 0.0f), transform, Eigen::Matrix4f::Identity());

    _skeleton.addBone(b0);
    _skeleton.addBone(b1);
}

float Mesh::step(float edge0, float edge1, float x)
{
    float t = glm::clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.f);
    return t;
}

float Mesh::smoothstep(float edge0, float edge1, float x)
{
    float t = glm::clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.f);
    return t * t * (3.0f - 2.0f * t);
}

void Mesh::computeLBSWeights(float jointInfluenceRange)
{
    _glBoneIndices.clear();
    float joint_min = 0.5f - jointInfluenceRange/2.0f;
    float joint_max = 0.5f + jointInfluenceRange/2.0f;

    // Compute vertices weights for each bone (currently based on UVs)
    for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
    {
        Vector3f vertex(point(*v_it).data());
        auto& vWeights = property(_vertexWeights, *v_it);

        auto bones = _skeleton.bones();
        float u = texcoord2D(*v_it)[0];

//        float w1 = smoothstep(joint_min, joint_max, u);
        float w1 = step(joint_min, joint_max, u);
        float w0 = 1.0f - w1;
        vWeights[0] = w0;
        vWeights[1] = w1;
        vWeights[2] = 0.0f;
        vWeights[3] = 0.0f;

        _glBoneIndices.emplace_back(0);
        _glBoneIndices.emplace_back(1);


        // For each bone
//        for (int i = 0; i < _skeleton.bones().size(); ++i)
//        {
//            Bone& bone = _skeleton.bones()[i];
//            // Compute the distance to the bone as weight
//            std::array<Vector3f, 2> endpoints = bone.endpoints();
//            Vector3f boneDir = endpoints[1] - endpoints[0];
//            float d = distOnBone(bone, vertex);
//            float totalDist = _skeleton.lengthTo(i - 1) + d;

//            float dist = distToSegment(endpoints[0], endpoints[1], vertex);
//            float w = 1.0f / dist;

//            vWeights.emplace_back(d);
//            weightSum += d  ;
//        }
//        std::cout << std::endl;
//        // Normalize weights
//        for (float& w : vWeights) {
//            w /= weightSum;
//        }
    }
}

void Mesh::sendBlendingMatricesToShader(Shader &shader) {
    for (unsigned int i = 0; i < _skeleton.bones().size(); ++i) {
        std::string blendmat_index = "blendingmatrices["+std::to_string(i)+"]";
        GLint weight_loc = shader.getUniformLocation(blendmat_index.c_str());
        glUniformMatrix4fv(weight_loc, 1, GL_FALSE, _skeleton.bones()[i].transform().data());
    }
}

void Mesh::initLBS()
{
    // Set mesh skeleton
    computeSkeleton();
}

void Mesh::linearBlendSkinning(float jointInfluenceRange)
{
    // Update weights
    _glBoneIndices.clear();
    computeLBSWeights(jointInfluenceRange);

    // For each vertex
    for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
    {
        Eigen::Matrix4f blendMatrix = Eigen::Matrix4f::Zero();
        Vector4f vWeights = property(_vertexWeights, *v_it);

        // For each bone
        for (int i = 0; i < _skeleton.bones().size(); ++i)
        {
            Bone b = _skeleton.bones()[i];
            blendMatrix += b.blendingMatrix() * vWeights[i];
        }

        Eigen::Vector4f vpos(point(*v_it)[0], point(*v_it)[1], point(*v_it)[2], 1.0f);
        Vector3f vnormal(normal(*v_it).data());

        vpos = blendMatrix * vpos;
        vnormal = blendMatrix.block<3,3>(0, 0).inverse().transpose() * vnormal;
        set_point(*v_it, Point(vpos[0], vpos[1], vpos[2]));
        set_normal(*v_it, Normal(vnormal[0], vnormal[1], vnormal[2]));
    }
    _LBSComputed = true;

    _skeleton.updateTransforms();
	glCheckError();
    updateVBO();
}

Vector3f Mesh::getCenter()
{
    return _bbox.center();
}
