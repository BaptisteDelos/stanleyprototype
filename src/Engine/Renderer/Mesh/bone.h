#ifndef BONE_H
#define BONE_H

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <Engine/Renderer/opengl_stuff.h>
#include <Engine/Renderer/Shader/shader.h>

using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;
using Vector4f = Eigen::Vector4f;
using Matrix4f = Eigen::Matrix4f;

const float degreeRadianRatio = M_PI/180.f;


class Bone
{
public:
    Bone(Vector3f v0, Vector3f v1, Matrix4f transform, Matrix4f refPose);

    void draw(Shader &shader);

    std::array<Vector3f, 2> endpoints() const;
    Matrix4f transform() const;
    Matrix4f blendingMatrix() const;
    float length(void) const;
    void updateCurrentTransform(void);

private:
    Matrix4f _transform;
    Matrix4f _referencePose;
    Matrix4f _blendingMatrix;
    Matrix4f _currentTransform;
    std::array<Vector3f, 2> _endpoints;

};

#endif // BONE_H
