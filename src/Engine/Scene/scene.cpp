#include "scene.h"
#include <iostream>


Scene::Scene(int width, int height) : _width(width), _height(height), _drawfill(true) {
    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
}

Scene::~Scene() {

}

void Scene::resize(int width, int height) {
   _width = width;
   _height = height;
   glViewport(0, 0, width, height);
}

void Scene::draw() {
    glCheckError();

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    if (_drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

}

void Scene::mouseclick(int , float , float ) {

}

void Scene::mousepressed(GLFWwindow *window, int button, int action, int mods) {

}

void Scene::mousemove(float xpos, float ypos) {

}

void Scene::mousescroll(float xpos, float ypos) {

}

void Scene::keyboardmove(int , double ) {

}

bool Scene::keyboard(unsigned int key) {

}

bool Scene::keyevent(int key, int scancode, int action, int mods) {

}

void Scene::toggledrawmode() {
    _drawfill = !_drawfill;
}

void Scene::setOpenGLFrameBuffer(GLuint fbo) {
    _defaultFramebufferObject = fbo;
}
