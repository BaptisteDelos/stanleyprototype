#ifndef BSPLINESURFACE_H
#define BSPLINESURFACE_H

#include "bsplinecurve.h"

class BSplineSurface {
public:
    BSplineSurface(std::vector< std::vector<Vector3f> >& grid, int order);
    ~BSplineSurface();

    void draw(void);
    void updateVBO(void);

    void computeSurface(float ustep, float vstep);
    Vector3f computePosition(float u, float v);
    Vector3f computeNormal(Vector3f p, float u, float v, float delta_u, float delta_v);

    std::vector<GLfloat> getVertices() const;

private:
    int _order;
    std::vector< std::vector<Vector3f> > _grid;
    std::vector<BSplineCurve*> _guideCurves;
    BSplineCurve* _generatorCurve;

    std::vector<GLfloat> _vertices;
    std::vector<GLfloat> _normals;
    std::vector<GLuint> _indices;

    // OpenGL objects for geometry
    GLuint _vao;
    GLuint _vbo;
    GLuint _nbo;
    GLuint _ebo;
};

#endif // BSPLINESURFACE_H
