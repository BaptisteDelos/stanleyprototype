﻿#ifndef MAINSCENE_H
#define MAINSCENE_H

#include "Engine/Scene/scene.h"
#include "Engine/Camera/camera.h"
#include "Core/BSpline/bsplinecurve.h"
#include "Core/BSpline/bsplinesurface.h"

#include <Engine/Renderer/Shader/shader.h>
#include <Engine/Renderer/Model/model.h>

#include <functional>
#include <memory>


class MainScene : public Scene {
public:
    explicit MainScene(int width, int height);
    ~MainScene();

    void resize(int width, int height) override;
    void draw() override;

    void mousepressed(GLFWwindow *window, int button, int action, int mods) override;
    void mouseclick(int button, float xpos, float ypos) override;
    void mousemove(float xpos, float ypos) override;
    void mousescroll(float xpos, float ypos) override;
    void keyboardmove(int key, double time) override;
    bool keyboard(unsigned int key) override;
    bool keyevent(int key, int scancode, int action, int mods) override;

private:
    Model _meshmodel;

    GLuint _quadVAO;
    GLuint _quadVBO;
    GLfloat _quadVertices[30] = {
        // Positions        // UV
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
        -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0f
    };

    // G-buffer textures
    GLuint _gPosition;
    GLuint _gNormal;
    GLuint _gAlbedoSpec;

    // Screen and SSAO textures
    GLuint _screenTexture;
    GLuint _noiseTexture;
    GLuint _ssaoColorBuffer;
    GLuint _ssaoColorBufferBlur;

    GLuint _screenFBO;
    GLuint _ssaoFBO;
    GLuint _ssaoBlurFBO;
    GLuint _gBuffer;
    GLuint _rBuffer;
    GLuint _rScreenBuffer;
    GLuint _ssaoRBuffer;

    // Shader programs for rendering
    Shader _programdefault;
    Shader _programpolygoncolor;
    Shader _program;
    Shader _programnormal;
    Shader _programdiffuse;
    Shader _programSSAO;
    Shader _programGBuffer;
    Shader _programLighting;
    Shader _programSSAOBlur;
    Shader _programFXAA;
    Shader _programWeights;

    enum ProgramType { DEFAULT=0, NORMAL, BLINN, GBUFFER, SSAO, LIGHTPASS, BLUR, FXAA, LBS };
    std::vector<std::string> _shaderPaths
    {
            DATA_DIR"/shaders/default",
            DATA_DIR"/shaders/normal",
            DATA_DIR"/shaders/blinn",
            DATA_DIR"/shaders/gbuffer",
            DATA_DIR"/shaders/ssao",
            DATA_DIR"/shaders/lightpass",
            DATA_DIR"/shaders/blur",
            DATA_DIR"/shaders/fxaa",
            DATA_DIR"/shaders/lbs",
    };

    bool _fxaa = true;

    // for mouse management
    int _button; // 0 --> left. 1 --> right. 2 --> middle. 3 --> other
    float _mousex{0};
    float _mousey{0};

    enum TrackMode
    {
      TM_NO_TRACK=0, TM_ROTATE_AROUND, TM_ZOOM,
      TM_LOCAL_ROTATE, TM_FLY_Z, TM_FLY_PAN,
      TM_TRANSLATE
    };
    TrackMode _trackingMode = TM_NO_TRACK;

    // Camera
    using CameraSelector=std::function<Camera*()>;
    std::vector<CameraSelector> _cameraselector;
    int _activecamera;

    std::unique_ptr<Camera> _camera;

    // matrices
    glm::mat4 _model;
    glm::mat4 _view;
    glm::mat4 _projection;

    std::vector<Vector3f> genSSAOKernel(int nsamples);
    std::vector<Vector3f> genSSAONoise(int noisewidth);
    void sendKernelSamplesToShader(int nsamples, Shader &shader);
    void initShaders(void);
    void initGBuffer(void);
    void initScreenRendering(void);
    void initSSAO(void);
    void drawQuad();

    Mesh makeCylinder(Vector3f base=Vector3f(0,0,0), Vector3f axis = Vector3f(1,0,0), float radius = .5f, float length = 3., int subdiv1 = 8, int subdiv2 = 16);
};

#endif // MAINSCENE_H
